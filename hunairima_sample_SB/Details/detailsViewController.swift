//
//  HistoryViewController.swift
//  hunairima_sample_SB
//
//  Created by yuya on 2020/11/26.
//

import UIKit
import SafariServices

private var titleText = ""
private var overviewText = ""
private var safariVC = SFSafariViewController(url: URL(string: "https://www.google.com/")!)

class detailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var detailsImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
        
    var details: [String] = [titleText, overviewText, "", ""]
    var landmarkText:String = ""
    var pictgramText:String = ""
    
    let Landmarks: [[String]] = [["history","歴史","西からの波浪から船舶を保護することを目的として、明治29年に函館市区営による「函館港改良工事」が始められました。","https://www.google.com/"],
                                 ["structure","構造","○船入澗防波堤（石積防波堤）は明治32年に完成し、基礎ブロックは日本人施工による最古のコンクリートブロックとされている。○石材は旧弁天岬台場解体時の発生材が使用されており、土木学会推奨土木遺産「函館港改良施設群」に選定されている。","https://www.google.com/"],
                                 ["construction","施工","○旧弁天岬台場跡地で製作されたコンクリートブロックは、軌道起重機で台車に載せて運搬しました。○桟橋に設置してある起重機で台船に積み降ろして搬出、浮起重機で吊り下げ、所定の位置に沈設しました。","https://www.google.com/"],
                                 ["repair","補修","○船入澗防波堤は、今もなお現役として機能し続けている一方で、一部に欠損、崩落が発生していたことから平成23年8月から修復工事に着手、平成25年3月に完成しました。","https://www.google.com/"],
                                 ["current","現在","○船入澗防波堤（石積防波堤）を含む「函館港改良施設群」は土木学会選奨土木遺産に選定されている。○土木遺産カードの発行や、歴史探訪ツアーへの活用など、都市漁村交流に貢献している。","https://www.google.com/"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        
        print(landmarkText)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        //セルの高さを自動設定
        cell.textLabel?.numberOfLines=0
        
        for i in 0 ..< Landmarks.count{
            if(landmarkText == Landmarks[i][0]){
                self.detailsImage.image = UIImage(named: Landmarks[i][0] + "01")
                safariVC = SFSafariViewController(url: URL(string: Landmarks[i][3])!)
                details = ["名称 : " + Landmarks[i][1],"概要 : \n" + Landmarks[i][2], "詳細を見る", ""]
            }
        }
            
        cell.textLabel!.text = details[indexPath.row]
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 2:
            self.present(safariVC, animated: true, completion: nil)
        default:
            break
        }
    }
    
}


