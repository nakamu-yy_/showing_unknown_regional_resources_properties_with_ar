import UIKit
import ARKit

class ViewController: UIViewController {
    @IBOutlet var sceneView: ARSCNView!
    
    // NOTE: The imageConfiguration is better for tracking images,
    // but it has less features,
    // for example it does not have the plane detection.
    let defaultConfiguration: ARWorldTrackingConfiguration = {
        let configuration = ARWorldTrackingConfiguration()
        
        let images = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
        configuration.detectionImages = images
        configuration.maximumNumberOfTrackedImages = 1
        return configuration
    }()
    
    let imageConfiguration: ARImageTrackingConfiguration = {
        let configuration = ARImageTrackingConfiguration()
        
        let images = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil)
        configuration.trackingImages = images!
        configuration.maximumNumberOfTrackedImages = 1
        return configuration
    }()
    
    
    private var buttonNode: SCNNode!
    
    private let feedback = UIImpactFeedbackGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        
        buttonNode = SCNScene(named: "art.scnassets/SceneKit Scene.scn")!.rootNode.childNode(withName: "button", recursively: false)
        
        feedback.prepare()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sceneView.session.run(imageConfiguration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: sceneView),
            let result = sceneView.hitTest(location, options: nil).first else {
                return
        }
        let node = result.node
        
        // ①storyboardのインスタンス取得
        let storyboard: UIStoryboard = self.storyboard!
        // ②遷移先ViewControllerのインスタンス取得(identifierの名前を間違うとフリーズ起こす)
        let toDetails = storyboard.instantiateViewController(withIdentifier: "detailsVC") as! detailsViewController
        
        //if~すいっちがおされたら
        switch node.name {
        case "what_is_it_now":
            toDetails.landmarkText = node.name!
            self.present(toDetails, animated: true, completion: nil)
        case "history":
            toDetails.landmarkText = node.name!
            self.present(toDetails, animated: true, completion: nil)
        case "tecjnology":
            toDetails.landmarkText = node.name!
            self.present(toDetails, animated: true, completion: nil)
        case "landscape":
            toDetails.landmarkText = node.name!
            self.present(toDetails, animated: true, completion: nil)
        case "hidden_charm":
            toDetails.landmarkText = node.name!
            self.present(toDetails, animated: true, completion: nil)
        default:
            break
        }
        
    }
    
}


extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        guard let imageAnchor = anchor as? ARImageAnchor else {
            return nil
        }
        
        switch imageAnchor.referenceImage.name {
        case "aviation_hakodate_gyokou" :
            DispatchQueue.main.async {
                self.feedback.impactOccurred()
            }
            //buttonNode.scale = SCNVector3(0.1, 0.1, 0.1)
            return buttonNode
            
        default:
            return nil
        }
    }
}
